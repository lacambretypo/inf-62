Ce repository est un espace de travail versionné pour le projet "∞ moins 62" du Master 2 de l'atelier de typographie de la Cambre lancé en september 2020. 

Le briefing de départ est le suivant :

"
Projet Frankenstein
# ∞ moins 62

## Le projet
1. Travailler sur une fonte.
2. Cette fonte est polymorphe.
3. Elle ne comporte pas de signes alphanumériques donc ni a→z ni A→Z ni 0→9.= 62 signes
4. Les accents (plus exactement les diacritiques) sont encouragés, mais décomposés, c'est à dire les accents seuls sans leur lettre de base. Par exemple, l'accent grave ` seul et pas le à.
5. Cette fonte est reliée à votre recherche en cours ou renaissante. C'est un aspect de votre recherche. C'est un moyen de faire un focus, de creuser profond plutôt que large. C'est une fonte qui servira aussi pour publier du texte.
6. Cette fonte doit être prête pour le mercredi 7 octobre à 17h lundi 12 octobre à 15h, où on fera un petit goûter festif avec les M1 et les bachelors et où on la publiera en ligne selon une licence qu'on aura discuté.
7. Dès que possible, on va la faire collectivement, on va rassembler toutes vos recherches et fichiers dans une seule fonte. Et pour arriver à faire ça, on va négocier entre nous et avec les techniques logicielles que nécessite une fonte produite collectivement, dont git.
8. On y injectera les lettres et chiffres a→z A→Z 0→9 (ce qui produira aussi les lettres accentuées) selon un protocole à définir (peut-être qu'elles seront multiples. Mais on s'intéressera à la licence des lettres qu'on injecte pour que le résultat soit publiable, voir https://pads.domainepublic.net/p/fontes-libres
9. Les glyphes que vous avez produit et qui sont intégrés dans cette fonte seront utilisés par d'autres que vous, entre autres avec Milady. On prendra tous soin des un-es et des autres donc.

## Petit exercice
On ferme les yeux, et on oublie la typographie pour quelques secondes, oui oui on oublie Gutenberg, le graphisme, cinq siècles d'imprimerie, tout ça! Et on regarde bien une fonte pour ce quelle est :
* une collection de formes (appelées glyphes) - https://fr.wikipedia.org/wiki/Glyphe
* ces glyphes sont combinables entre eux dans des compositions (ce sont de petits blocs assemblables) mais ils ménagent aussi des espaces entre eux
* ces glyphes ont des codes (unicode) et des noms, et sont mobilisables par l'extérieur (par un texte par exemple, et/ou le clavier) - https://fr.wikipedia.org/wiki/Texte#En_informatique
* par les fonctionnalités opentype (features), on peut les computer. Un exemple simple : f + i donne ﬁ - mais on peut programmer des substitutions ou des modifications très complexes aussi - https://en.wikipedia.org/wiki/OpenType#OpenType_support
* les fontes variables permettent au besoin de stocker des objects plus complexes que le contour monochrome habituel, et de les faire varier selon différents critères - https://en.wikipedia.org/wiki/Variable_fonts
*  en combinant plusieurs fontes dans une "famille", on peut exprimer autrement plusieurs fois le même glyphe. Mais on peut en fait, par les fonctionnalités, combiner plusieurs fois le même glyphe dans une fonte. En utilisant cette fonte dans un logiciel de mise en page ou sur le web, on peut la recombiner encore autrement. Si on utilise les glyphes de l'un-e de vous combiné avec les glyphes d'un-e autre, qu'est ce qui est produit par cette rencontre?

## Étapes

Pour ce lundi 21/9 :

Quels sont les différentes possibilités, en trouver au minimum 5 pour voir laquelle poursuire: partir de différentes choses: des détails, des relevés, des collections, des séries. C'est très différent des pictogrammes, des emojis. Faire cette collecte visuellement, et par le dessin. Pas de long discours ou blabla :)

## Références
* Images et pdf sur https://galee.lacambretypo.be/s/88dALfg2MeHDZxA
* Unicode - https://home.unicode.org/
* Unicode - tous les "charts" (le coeur du standard) https://www.unicode.org/charts
* Unicode expliqué par lui-même - https://www.youtube.com/watch?v=-n2nlPHEMG8
* Decode Unicode, le projet universitaire et graphique - https://decodeunicode.org/
* avec des infos pour chaque glyphe et script et langue et bloc - https://decodeunicode.org/en/u+1D2E0
* Missing Scripts, projet avec l'ANRT et universités de Mainz et Berkeley - http://www.theworldswritingsystems.org/ et https://vimeo.com/325173699
* Une fonte avec fonctionnalités opentype peu courantes - https://github.com/aftertheflood/sparks
* Publications sur la table le 16/09/2020 - https://galee.lacambretypo.be/s/rtXC3oHQfzy7mQb et https://galee.lacambretypo.be/s/QaTX74o2g6C9Xwo
* Des posts sur unicode dans le blog zigzag animal de l'Erg - http://zigzaganimal.be/?s=unicode

"

Fin octobre, chaque participant a produit sa fonte, et nous avons débattu de la mise en commun de tout ces glyphes. Il a été décidé d'initier la fonte à partir de la version de Julie Lemoine, la seule dont le projet requiert des glyphes alphanumériques, puisés dans la "Happy Times at the IKOB" de Lucas Le Bihan publiée sous licence libre OFL chez VTF.

Pour la suite:

On échange les fichiers en UFO.
Ceux qui travaillent sur Glyphs gardent leur fichier source, et idem pour FontForge.

Julie : rajouter des glyphes et rajouter les fonctions, déja écrites.

Marie : ajouter des glyphes alternate pour la ponctuation et accents et points sur les i pour les relier, corriger les fonctionnalités pour que les variations a-b-c fonctionnent.

Adrien : vectoriser tout, glyphes autonomes, sans codage unicode, avec des noms, et des fonctionnalités opentype

Mathilde : en stage - un ajout?